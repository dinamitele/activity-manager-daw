﻿using ActivityManager.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace ActivityManager.Controllers
{
    [Authorize]
    public class TaskController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        public ActionResult GetAll()
        {
            var tasks = db.Tasks;
            ViewBag.Tasks = tasks;
            return View();
        }

        public ActionResult Show(int id)
        {
            bool isMember = false;
            bool isOrganizer = false;

            Task task = db.Tasks.Include(i => i.TaskStatus).Include(i => i.CreatedBy).Include(i => i.AssignedTo)
                .SingleOrDefault(x => x.Id == id);
            
            GetRoleInTeam(ref isMember, ref isOrganizer, task.ProjectId);

            if ( isMember || isOrganizer || User.IsInRole("Administrator"))
            {
                if (TempData.ContainsKey("message"))
                {
                    ViewBag.message = TempData["message"].ToString();
                }
                task.Comments = db.Comments.Where(x => x.TaskId == id).Include(x => x.User);
                ViewBag.Task = task;
                ViewBag.afisareButoane = false;
                if (isOrganizer || User.IsInRole("Administrator"))
                {
                    ViewBag.afisareButoane = true;
                }
                ViewBag.esteAdmin = User.IsInRole("Administrator");
                ViewBag.utilizatorCurent = User.Identity.GetUserId();
                return View(task);
            }
            else
            {
                TempData["message"] = "You don't have the right to see this task!";
                return Redirect("~");
            }
        }

        public ActionResult New(int? projectId)
        {
            if (projectId != null)
            {
                Task task = new Task
                {
                    Projects = GetAllProjects() //vor trebui aduse doar proiectele echipei din care este membru
                };

                bool isMember = false;
                bool isOrganizer = false;
                GetRoleInTeam(ref isMember, ref isOrganizer, projectId ?? 0);

                if (isOrganizer || User.IsInRole("Administrator"))
                {
                        task.ProjectId = projectId.Value;
                        return View(task);
                }
                else
                {
                        TempData["message"] = "You don't have the right to create a new task!";
                        return Redirect("~");
                }
            }
            TempData["message"] = "You need to select a project first!";
            return Redirect("~");
        }

        [HttpPost]
        public ActionResult New(Task task)
        {
            bool isMember = false;
            bool isOrganizer = false;
            GetRoleInTeam(ref isMember, ref isOrganizer, task.ProjectId);

            if (isOrganizer || User.IsInRole("Administrator"))
            {
                task.Projects = GetAllProjects(); //vor trebui aduse doar proiectele echipei din care este membru
                try
                {
                    if (ModelState.IsValid)
                    {
                        task.CreatedOn = DateTime.Now;
                        task.StartDate = null;
                        task.EndDate = null;
                        task.TaskStatusId = 1;
                        task.CreatedById = User.Identity.GetUserId();
                        task.AssignedToId = null;
                        //task.TaskStatuses = GetAllTaskStatuses();
                        db.Tasks.Add(task);
                        db.SaveChanges();
                        TempData["message"] = "The task has been created!";
                        return RedirectToAction("Show", "Project", new { id = task.ProjectId });
                    }
                    else
                    {
                        return View(task);
                    }
                }
                catch (Exception e)
                {
                    System.Console.WriteLine(e);
                    return View(task);
                }
            }
            else
            {
                TempData["message"] = "You don't have the right to create a new task!";
                return Redirect("~");
            }
        }

        public ActionResult Edit(int id)
        {
            Task task = db.Tasks.Where(x => x.Id == id).Include(x => x.Project).FirstOrDefault();

            bool isMember = false;
            bool isOrganizer = false;
            GetRoleInTeam(ref isMember, ref isOrganizer, task.ProjectId);

            if (isOrganizer|| isMember || User.IsInRole("Administrator"))
            {
                task.Projects = GetAllProjects(); //vor trebui aduse doar proiectele echipei din care este membru
                task.TaskStatuses = GetAllTaskStatuses();
                task.TeamPersons = GetTeamPersons(task.Project.TeamId);
                task.Comments = db.Comments.Where(x => x.TaskId == id);
                ViewBag.Task = task;
                // daca este doar membru, poate modifica doar statusul
                ViewBag.SeeAll = (isOrganizer || User.IsInRole("Administrator"));
                return View(task);
            }
            else
            {
                TempData["message"] = "You don't have the right to edit this task!";
                return Redirect("~");
            }
        }

        [HttpPut]
        public ActionResult Edit(int id, Task requestTask)
        {
            Task task = db.Tasks.Where(x => x.Id == id).Include(x => x.Project).FirstOrDefault();
            requestTask.Projects = GetAllProjects(); //vor trebui aduse doar proiectele echipei din care este membru
            requestTask.TaskStatuses = GetAllTaskStatuses();
            requestTask.TeamPersons = GetTeamPersons(task.Project.TeamId);

            bool isMember = false;
            bool isOrganizer = false;
            GetRoleInTeam(ref isMember, ref isOrganizer, task.ProjectId);
            ViewBag.SeeAll = (isOrganizer || User.IsInRole("Administrator"));

            try
            {
                if (ModelState.IsValid)
                {
                    task.Projects = GetAllProjects(); //vor trebui aduse doar proiectele echipei din care este membru
                    task.TaskStatuses = GetAllTaskStatuses();
                    task.TeamPersons = GetTeamPersons(task.Project.TeamId);
                    if (requestTask.StartDate > requestTask.EndDate)
                    {
                        TempData["error"] = "The start date cannot be set after the end date.";
                        //                        ViewBag.message = TempData["error"].ToString();
                        return View(requestTask);
                    }
                    //if ((requestTask.TaskStatusId == 1 && (requestTask.StartDate != null || requestTask.EndDate != null))
                    //    || (requestTask.TaskStatusId == 2 && (requestTask.StartDate == null || requestTask.EndDate != null))
                    //    || (requestTask.TaskStatusId == 3 && (requestTask.StartDate == null || requestTask.EndDate == null)))
                    //{
                    //    TempData["message"] = "Datele introduse nu sunt corecte!";
                    //    return View(requestTask);
                    //}
                    task.Comments = db.Comments.Where(x => x.TaskId == id);
                    if (TryUpdateModel(task))
                    {
                        task.Title = requestTask.Title;
                        task.Description = requestTask.Description;
                        task.StartDate = requestTask.StartDate;
                        task.EndDate = requestTask.EndDate;
                        task.TaskStatusId = requestTask.TaskStatusId;
                        task.AssignedToId = requestTask.AssignedToId;
                        db.SaveChanges();
                        TempData["message"] = "The task has been edited!";
                    }
                    return RedirectToAction("Show", new { id = task.Id });
                }
                else
                {
                    return View(requestTask);
                }

            }
            catch (Exception e)
            {
                return View(requestTask);
            }
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            Task task = db.Tasks.Find(id);

            bool isMember = false;
            bool isOrganizer = false;
            GetRoleInTeam(ref isMember, ref isOrganizer, task.ProjectId);

            if ((task.CreatedById == User.Identity.GetUserId() && isOrganizer )|| User.IsInRole("Administrator"))
            {
                db.Tasks.Remove(task);
                db.SaveChanges();
                TempData["message"] = "The task was deleted!";
                return RedirectToAction("Show", "Project", new { id = task.ProjectId });
            }
            else
            {
                TempData["message"] = "You don't have the right to delete this task!";
                return Redirect("~");
            }
        }

        [NonAction]
        public IEnumerable<SelectListItem> GetAllProjects()
        {
            var selectList = new List<SelectListItem>();
            string userId = User.Identity.GetUserId();
            var teamsIds = db.UserTeams.Where(x => x.UserId == userId).Select(x => x.TeamId);
            var projects = db.Projects.Where(x => teamsIds.Contains(x.TeamId));
            foreach (var project in projects)
            {
                selectList.Add(new SelectListItem
                {
                    Value = project.Id.ToString(),
                    Text = project.Name
                });
            }
            return selectList;
        }

        [NonAction]
        public IEnumerable<SelectListItem> GetAllTaskStatuses()
        {
            var selectList = new List<SelectListItem>();
            var taskStatuses = db.TaskStatus;
            foreach (var taskStatus in taskStatuses)
            {
                selectList.Add(new SelectListItem
                {
                    Value = taskStatus.Id.ToString(),
                    Text = taskStatus.Status
                });
            }
            return selectList;
        }

        [NonAction]
        public IEnumerable<SelectListItem> GetTeamPersons(int teamId)
        {
            var selectList = new List<SelectListItem>();
            Team team = db.Teams.Where(u => u.Id == teamId).FirstOrDefault();
            foreach (var userTeam in team.UserTeams)
            {
                selectList.Add(new SelectListItem
                {
                    Value = userTeam.User.Id,
                    Text = userTeam.User.Email
                });
            }
            return selectList;
        }

        [NonAction]
        private void GetRoleInTeam(ref bool isMember, ref bool isOrganizer, int projectId)
        {
            string userId = User.Identity.GetUserId();

            int teamId = db.Projects.Find(projectId).TeamId;
            Team team = db.Teams.Find(teamId);

            UserTeam userTeam = db.UserTeams.Where(x => x.UserId == userId && x.TeamId == teamId).FirstOrDefault();
            if (userTeam != null)
            {
                if (team.OrganizerId == userId && User.IsInRole("Organizer"))
                {
                    isOrganizer = true;
                }
                else
                {
                    if(User.IsInRole("Member"))
                        isMember = true;
                }                   
            }
        }

    }
}
