﻿using ActivityManager.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActivityManager.Controllers
{
    [Authorize]
    public class UserTeamController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


        public ActionResult New(int teamId)
        {
            Team team = db.Teams.Find(teamId);
            if ((team.OrganizerId == User.Identity.GetUserId() && User.IsInRole("Organizer")) || User.IsInRole("Administrator"))
            {
                UserTeam userTeam = new UserTeam
                {
                    AllUsers = GetAllUsers(teamId),
                    TeamId = teamId
                };
                return View(userTeam);
            }
            else
            {
                TempData["message"] = "You don't have the right to create a project!";
                return Redirect("~"); ;
            }

        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> New(UserTeam userTeam)
        {
            userTeam.AllUsers = GetAllUsers(userTeam.TeamId);
            try
            {
                if (ModelState.IsValid)
                {
                    db.UserTeams.Add(userTeam);
                    db.SaveChanges();

                    // Adaugam rolul de membru
                    ApplicationDbContext context = new ApplicationDbContext();
                    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

                    if (!UserManager.IsInRole(userTeam.UserId,"Member"))
                    {
                        UserManager.AddToRole(userTeam.UserId, "Member");
                        db.SaveChanges();
                    }

                    return RedirectToAction("Show", "Team", new { id = userTeam.TeamId });
                }
                else
                {
                    return View(userTeam);
                }
            }
            catch (Exception e)
            {

                return View(userTeam);
            }
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task<ActionResult> Delete(int teamId, string userId)
        {
            UserTeam userTeam = db.UserTeams.Where(x => x.UserId == userId && x.TeamId == teamId).FirstOrDefault();
            db.UserTeams.Remove(userTeam);
            db.SaveChanges();
            TempData["message"] = "User has been succesfully removed form this team";

            //Stergem rolul de membru 
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            List<UserTeam> otherUserTeams = db.UserTeams.Where(x => x.UserId == userId).ToList();
            if (otherUserTeams.Count() == 0)
            {
                UserManager.RemoveFromRole(userId, "Member");

                db.SaveChanges();
            }
            else
            {
                //Verificam daca nu cumva e numai organizator
                bool removeMember = true;
                foreach (var uT in otherUserTeams)
                {
                    Team team = db.Teams.Where(x => x.Id == uT.TeamId).First();
                    if (team.OrganizerId != userId)
                    {
                        removeMember = false;
                        break;
                    }
                }
                if (removeMember)
                {
                    UserManager.RemoveFromRole(userId, "Member");
                }

            }
            return RedirectToAction("Show", "Team", new { id = userTeam.TeamId });
        }

        [NonAction]
        public IEnumerable<SelectListItem> GetAllUsers(int teamId)
        {
            var selectList = new List<SelectListItem>();
            var users = db.Users;
            Team team = db.Teams.Where(u => u.Id == teamId).FirstOrDefault();
            var userIds = team.UserTeams.Select(x => x.UserId);
            foreach (var user in users)
            {
                if(!userIds.Contains(user.Id))
                {
                    selectList.Add(new SelectListItem
                    {
                        Value = user.Id,
                        Text = user.Email
                    });
                }             
            }
            return selectList;
        }
    }
}