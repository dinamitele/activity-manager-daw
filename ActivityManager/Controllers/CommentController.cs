﻿using ActivityManager.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActivityManager.Controllers
{
    [Authorize]
    public class CommentController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult New (int taskId = -1)
        {
            Comment comment = new Comment();
            if (taskId > -1)
            {
                bool isMember = false;
                bool isOrganizer = false;
                GetRoleInTeam(ref isMember, ref isOrganizer, taskId);

                if (isOrganizer || isMember || User.IsInRole("Administrator"))
                {
                    comment.TaskId = taskId;
                }
                else
                {

                    TempData["message"] = "You don't have the right to add a comment!";
                    return Redirect("~");
                }
            }
            return View(comment);
        }

        [HttpPost]
        public ActionResult New(Comment comment)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    comment.CreatedOn = DateTime.Now;
                    comment.UserId = User.Identity.GetUserId();
                    db.Comments.Add(comment);
                    db.SaveChanges();
                    TempData["message"] = "The comment has been created!";
                    return RedirectToAction("Show", "Task", new { id = comment.TaskId });
                }
                else
                {
                    return View(comment);
                }
            }
            catch (Exception e)
            {
                return View(comment);
            }
        }

        public ActionResult Edit(int id)
        {
            Comment comment = db.Comments.Find(id);
            if (comment.UserId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                ViewBag.Comment = comment;
                return View(comment);
            }
            else
            {
                TempData["message"] = "You don't have the right to edit this comment!";
                return Redirect("~");
            }
        }

        [HttpPut]
        public ActionResult Edit(int id, Comment requestComment)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Comment comment = db.Comments.Find(id);
                    if (TryUpdateModel(comment))
                    {
                        comment.Content = requestComment.Content;
                        db.SaveChanges();
                        TempData["message"] = "The comment has been edited!";
                    }
                    return RedirectToAction("Show", "Task", new { id = comment.TaskId });
                }
                else
                {
                    return View(requestComment);
                }
            }
            catch (Exception e)
            {
                return Redirect("~");
            }
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            Comment comment = db.Comments.Find(id);

            if (comment.UserId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                db.Comments.Remove(comment);
                db.SaveChanges();
                TempData["message"] = "The comment has been deleted!";
                return RedirectToAction("Show", "Task", new { id = comment.TaskId });
            }
            else
            {
                TempData["message"] = "You cannot delete this comment!";
                return Redirect("~");
            }

        }

        [NonAction]
        private void GetRoleInTeam(ref bool isMember, ref bool isOrganizer, int taskId)
        {
            string userId = User.Identity.GetUserId();
            Task task = db.Tasks.Find(taskId);
            int teamId = db.Projects.Find(task.ProjectId).TeamId;
            Team team = db.Teams.Find(teamId);

            UserTeam userTeam = db.UserTeams.Where(x => x.UserId == userId && x.TeamId == teamId).FirstOrDefault();
            if (userTeam != null)
            {
                if (team.OrganizerId == userId && User.IsInRole("Organizer"))
                {
                    isOrganizer = true;
                }
                else
                {
                    if (User.IsInRole("Member"))
                        isMember = true;
                }

            }
        }
    }
}