﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActivityManager.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            ViewBag.isAdmin = User.IsInRole("Administrator");
            ViewBag.isUser = User.IsInRole("User");
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}