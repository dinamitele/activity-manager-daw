﻿using ActivityManager.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActivityManager.Controllers
{
    [Authorize (Roles = "Administrator")]
    public class UserController : Controller
    {
        private ApplicationDbContext db = ApplicationDbContext.Create();

        // GET: User
        public ActionResult Index()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var users = from user in db.Users orderby user.UserName select user;
            foreach (var user in users)
            {
                user.UserRoles = UserManager.GetRoles(user.Id).ToList();
                user.OrganizerTeams = db.Teams.Where(x => x.OrganizerId == user.Id).ToList();
                var organizerTeamsIds = user.OrganizerTeams.Select(x => x.Id);
                var memberTeamsIds = db.UserTeams.Where(x => x.UserId == user.Id && !organizerTeamsIds.Contains(x.TeamId)).Select(x => x.TeamId);
                user.MemberTeams = db.Teams.Where(x => memberTeamsIds.Contains(x.Id)).ToList();
            }
            
            ViewBag.UsersList = users;
            return View();
        }

        public ActionResult Edit(string id)
        {
            ApplicationUser user = db.Users.Find(id);
            user.AllRoles = GetAllRoles(id);
            var userRole = user.Roles.FirstOrDefault();
            ViewBag.userRole = userRole.RoleId;
            return View(user);
        }

        [HttpPut]
        public ActionResult Edit(string id, ApplicationUser newData)
        {
            ApplicationUser user = db.Users.Find(id);
            try
            {
                if (TryUpdateModel(user))
                {
                    user.UserName = newData.UserName;
                    user.Email = newData.Email;
                    user.PhoneNumber = newData.PhoneNumber;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(user);
            }
        }

        [HttpDelete]
        public ActionResult RemoveRole(string id, string roleName, int? teamId)
        {
            ApplicationUser user = db.Users.Find(id);
            user.OrganizerTeams = db.Teams.Where(x => x.OrganizerId == user.Id).ToList();
            var organizerTeamsIds = user.OrganizerTeams.Select(x => x.Id);
            var memberTeamsIds = db.UserTeams.Where(x => x.UserId == user.Id && !organizerTeamsIds.Contains(x.TeamId)).Select(x => x.TeamId);
            user.MemberTeams = db.Teams.Where(x => memberTeamsIds.Contains(x.Id)).ToList();
            try
            {
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                if (roleName == "Organizer")
                {
                    if(user.OrganizerTeams.Count() == 1)
                    {
                        UserManager.RemoveFromRole(id, roleName);
                    }
                    var team = db.Teams.Where(x => x.Id == teamId.Value).FirstOrDefault();
                    team.OrganizerId = User.Identity.GetUserId();
                }
                else if (roleName == "Member")
                {
                    if (user.MemberTeams.Count() == 1)
                    {
                        UserManager.RemoveFromRole(id, roleName);
                    }
                    UserTeam userTeam = db.UserTeams.Where(x => x.UserId == id && x.TeamId == teamId).FirstOrDefault();
                    db.UserTeams.Remove(userTeam);
                }
                else
                {
                    UserManager.RemoveFromRole(id, roleName);
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return View(user);
            }
        }

        public ActionResult AddRole(string userId)
        {
            NewRole newRole = new NewRole
            {
                UserId = userId,
                AllRoles = GetAllRoles(userId)
            };
            ViewBag.OrganizerId = db.Roles.Where(x => x.Name == "Organizer").Select(x => x.Id).FirstOrDefault();

            ViewBag.MemberId = db.Roles.Where(x => x.Name == "Member").Select(x => x.Id).FirstOrDefault();

            return View(newRole);
        }

        [HttpPost]
        public ActionResult AddRole(string userId, string roleId, int? teamId)
        {
            var organizerId = db.Roles.Where(x => x.Name == "Organizer").Select(x => x.Id).FirstOrDefault();

            var memberId = db.Roles.Where(x => x.Name == "Member").Select(x => x.Id).FirstOrDefault();

            NewRole newRole = new NewRole
            {
                UserId = userId
            };

            if ((roleId == organizerId || roleId == memberId) && teamId == 0)
            {
                var organizerTeams = db.Teams.Where(x => x.OrganizerId != userId).ToList();
                var organizerTeamsIds = db.Teams.Where(x => x.OrganizerId == userId).Select(x => x.Id); // in aceste echipe e organizator
                var memberTeamsIds = db.UserTeams.Where(x => x.UserId == userId).Select(x => x.TeamId);
                var memberTeams = db.Teams.Where(x => !memberTeamsIds.Contains(x.Id) && !organizerTeamsIds.Contains(x.Id)).ToList();

                var selectList = new List<SelectListItem>();

                if(roleId == organizerId)
                {
                    foreach (var team in organizerTeams)
                    {
                        selectList.Add(new SelectListItem
                        {
                            Value = team.Id.ToString(),
                            Text = team.Name
                        });
                    }
                    newRole.AvailableOrganizerTeams = selectList;
                }
                else
                {
                    foreach (var team in memberTeams)
                    {
                        selectList.Add(new SelectListItem
                        {
                            Value = team.Id.ToString(),
                            Text = team.Name
                        });
                    }
                    newRole.AvailableMemberTeams = selectList;
                }               
                ViewBag.OrganizerId = db.Roles.Where(x => x.Name == "Organizer").Select(x => x.Id).FirstOrDefault();
                ViewBag.MemberId = db.Roles.Where(x => x.Name == "Member").Select(x => x.Id).FirstOrDefault();
                newRole.RoleId = roleId;
                return View(newRole);
            }
            else try
            {
                    ApplicationUser user = db.Users.Find(userId);
                    ApplicationDbContext context = new ApplicationDbContext();
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    var role = db.Roles.Where(x => x.Id == roleId).FirstOrDefault();
                    user.UserRoles = UserManager.GetRoles(user.Id).ToList();
                    if (!user.UserRoles.Contains(role.Name))
                    {
                        UserManager.AddToRole(userId, role.Name);
                    }
                    if (role.Name == "Member")
                    {

                        UserTeam userTeam = new UserTeam
                        {
                            UserId = userId,
                            TeamId = teamId.Value
                        };
                        db.UserTeams.Add(userTeam);
                    }
                    else if (role.Name == "Organizer")
                    {
                        var team = db.Teams.Where(x => x.Id == teamId.Value).FirstOrDefault();
                        var formerOrganizerId = team.OrganizerId;
                        team.OrganizerId = userId;
                        var userInTeam = db.UserTeams.Where(x => x.TeamId == teamId.Value && x.UserId == userId).FirstOrDefault();
                        if (userInTeam == null)
                        {
                            UserTeam userTeam = new UserTeam
                            {
                                TeamId = teamId.Value,
                                UserId = userId
                            };
                            db.UserTeams.Add(userTeam);
                        }

                        db.SaveChanges();

                        var actualOrganizerOrganizedTeams = db.Teams.Where(x => x.OrganizerId == userId);
                        var actualOrganizerAllTeams = db.UserTeams.Where(x => x.UserId == userId);
                        if (actualOrganizerOrganizedTeams.Count() == actualOrganizerAllTeams.Count())
                        {
                            if (UserManager.IsInRole(userId, "Member"))
                            {
                                UserManager.RemoveFromRole(userId, "Member");
                            }
                        }

                        var formerOragnizerIsStillOragnizer = db.Teams.Where(x => x.OrganizerId == formerOrganizerId);
                        if (formerOragnizerIsStillOragnizer.Count()==0)
                        {
                            UserManager.RemoveFromRole(formerOrganizerId, "Organizer");
                        }

                        if (!UserManager.IsInRole(formerOrganizerId, "Member"))
                        {
                            UserManager.AddToRole(formerOrganizerId, "Member");
                        }
                    }
                    db.SaveChanges();
                    return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return View(newRole);
            }
        }

        [NonAction]
        public IEnumerable<SelectListItem> GetAllRoles(string userId)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var selectList = new List<SelectListItem>();
            var roles = from role in db.Roles select role;
            foreach (var role in roles)
            {
                if (UserManager.IsInRole(userId, role.Name) && (role.Name == "Administrator" || role.Name == "User"))
                    continue;
                selectList.Add(new SelectListItem
                {
                    Value = role.Id.ToString(),
                    Text = role.Name.ToString()
                });
            }
            return selectList;
        }
    }
}