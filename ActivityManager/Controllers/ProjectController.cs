﻿using ActivityManager.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace ActivityManager.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        public ActionResult GetAll()
        {
            List<Project> projects;
            projects = db.Projects.Include("User").ToList();
            ViewBag.Projects = projects;
            return View();
        }

        public ActionResult Show(int id)
        {
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            Project project = db.Projects.Include(i => i.User).SingleOrDefault(x => x.Id == id);
            project.Tasks = GetTaskForProject(project.Id);
            ViewBag.Project = project;
            return View(project);
        }

        public ActionResult New(int teamId)
        {
            Team team = db.Teams.Find(teamId);
            if ((team.OrganizerId == User.Identity.GetUserId() && User.IsInRole("Organizer")) || User.IsInRole("Administrator"))
            {
                Project project = new Project
                {
                    TeamId = teamId
                };
                return View(project);
            }
            else
            {
                TempData["message"] = "You don't have the right to create a project!";
                return Redirect("~");
            }
        }

        [HttpPost]
        public ActionResult New(Project project)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    project.CreatedOn = DateTime.Now;
                    project.UserId = User.Identity.GetUserId();
                    db.Projects.Add(project);
                    db.SaveChanges();
                    return RedirectToAction("Show", "Team", new { id = project.TeamId });
                }
                else
                {
                    return View(project);
                }
            }
            catch (Exception e)
            {
                return View(project);
            }
        }

        public ActionResult Edit(int id)
        {
            Project project = db.Projects.Find(id);
            //Verificam daca e organizatorul proiectului(echipei)
            if ((project.UserId == User.Identity.GetUserId() && User.IsInRole("Organizer")) ||User.IsInRole("Administrator"))
            {

                project.Tasks = GetTaskForProject(project.Id);
                ViewBag.Project = project;
                return View(project);
            }
            else
            {
                TempData["message"] = "You don't have the right to edit a project you haven't created!";
                return Redirect("~");
            }
        }

        [HttpPut]
        public ActionResult Edit(int id, Project requestProject)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Project project = db.Projects.Find(id);
                    project.Tasks = GetTaskForProject(project.Id);
                    if (TryUpdateModel(project))
                    {
                        project.Name = requestProject.Name;
                        project.Description = requestProject.Description;
                        db.SaveChanges();
                        TempData["message"] = "The project has been edited!";
                    }
                    return RedirectToAction("Show", new { id = project.Id });
                }
                else
                {
                    return View(requestProject);
                }
            }
            catch (Exception e)
            {
                return Redirect("~");
            }
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            Project project = db.Projects.Find(id);
            if ((project.UserId == User.Identity.GetUserId() && User.IsInRole("Organizer")) || User.IsInRole("Administrator"))
            {
                //Team team = db.Teams.Find(id);
                List<Task> tasks = db.Tasks.Where(x => x.ProjectId == project.Id).ToList();

                foreach (Task task in tasks)
                {
                    db.Tasks.Remove(task);
                }

                //db.Teams.Remove(team);
                db.Projects.Remove(project);
              
                db.SaveChanges();
                TempData["message"] = "The project was deleted";
                return RedirectToAction("Show", "Team", new { id = project.TeamId });
            }
            else
            {
                TempData["message"] = "You don't have the right to delete this project.!";
                return Redirect("~");
            }
        }

        [NonAction]
        public IEnumerable<Task> GetTaskForProject(int projectId)
        {
            var tasks = db.Tasks.Include(i => i.TaskStatus).Where(x => x.ProjectId==projectId).ToList();
            return tasks;
        }
    }
}