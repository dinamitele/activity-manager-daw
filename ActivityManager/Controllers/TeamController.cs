﻿using ActivityManager.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace ActivityManager.Controllers
{
    public class TeamController : Controller
    {
        private ApplicationDbContext db = ApplicationDbContext.Create();
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        // GET: Team
        [Authorize]
        public ActionResult Index()
        {
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            if (User.IsInRole("Administrator"))
            {
                var allTeams = db.Teams;
                ViewBag.Teams = allTeams;
                ViewBag.isAdmin = true;
            }
            else
            {
                var userId = User.Identity.GetUserId();
                var userTeamsIds = db.UserTeams.Where(x => x.UserId == userId).Select(x => x.TeamId);
                var teams = db.Teams.Where(x => userTeamsIds.Contains(x.Id));
                var userTeams = db.UserTeams.Where(x => x.UserId == userId);
                foreach (var team in teams)
                {
                    team.UserTeams = new List<UserTeam>();
                    team.UserTeams =  db.UserTeams.Where(x => x.TeamId == team.Id).ToList();

                }
                ViewBag.Teams = teams;
                ViewBag.isAdmin = false;
                ViewBag.UserId = User.Identity.GetUserId();
            }
            return View();
        }

        [Authorize]
        public ActionResult Show(int id)
        {
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }

            Team team = db.Teams.Include(i => i.Organzier).SingleOrDefault(x => x.Id == id);
            team.Projects = db.Projects.Where(x => x.TeamId == id);
            var projectIds = team.Projects.Select(x => x.Id);
            team.Tasks = db.Tasks.Where(x => projectIds.Contains(x.ProjectId));
            ViewBag.Team = team;
            if (User.IsInRole("Administrator") || (User.IsInRole("Organizer") && team.OrganizerId == User.Identity.GetUserId() ))
            {
                ViewBag.isOrganizerOrAdministrator = true;
            }
            else
            {
                ViewBag.isOrganizerOrAdministrator = false;
            }
            return View(team);
        }

        [Authorize]
        public ActionResult New(int id = 0)
        {
            Team team = new Team();
            return View(team);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> New(Team team)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    UserTeam userTeam = new UserTeam();
                    team.OrganizerId = User.Identity.GetUserId();
                    db.Teams.Add(team);
                    db.SaveChanges();
                    TempData["message"] = "Echipa a fost creata!";

                    userTeam.UserId = User.Identity.GetUserId();
                    userTeam.TeamId = team.Id;
                    userTeam.AllUsers = new List<SelectListItem>();
                    db.UserTeams.Add(userTeam);
                    db.SaveChanges();

                    //utilizatorul devine organizator 
                    ApplicationDbContext context = new ApplicationDbContext();
                    var roleManager = new RoleManager<IdentityRole>(new
                    RoleStore<IdentityRole>(context));
                    var UserManager = new UserManager<ApplicationUser>(new
                    UserStore<ApplicationUser>(context));

                    if (!User.IsInRole("Organizer"))
                    {
                        UserManager.AddToRole(team.OrganizerId, "Organizer");
                        db.SaveChanges();

                        Request.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);
                        var identity = await UserManager.CreateIdentityAsync((db.Users.Where(x => x.Id == team.OrganizerId)).First(), DefaultAuthenticationTypes.ApplicationCookie);
                        AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true }, identity);

                    }

                    return RedirectToAction("Index");
                }
                else
                {
                    return View(team);
                }
            }
            catch (Exception e)
            {
                return View(team);
            }
        }

        [Authorize]
        public ActionResult Edit(int id)
        {
            Team team = db.Teams.Find(id);

            if ((team.OrganizerId == User.Identity.GetUserId() && User.IsInRole("Organizer")) || User.IsInRole("Administrator"))
            {
                ViewBag.Team = team;
                return View(team);
            }
            else
            {
                TempData["message"] = "You don't have the right to edit this team!";
                return Redirect("~");
            }
        }

        [HttpPut]
        public ActionResult Edit(int id, Team requestTeam)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Team team = db.Teams.Find(id);
                    if (TryUpdateModel(team))
                    {
                        team.Name = requestTeam.Name;
                        db.SaveChanges();
                        TempData["message"] = "The changes were applied!";
                    }
                    return RedirectToAction("Show", new { id = team.Id });
                }
                else
                {
                    return View();
                }
            }
            catch (Exception e)
            {
                return View();
            }
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            Team team = db.Teams.Find(id);

            if (team.OrganizerId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                var projects = db.Projects.Where(x => x.TeamId == id);
                foreach(var project in projects)
                {
                    var tasks = db.Tasks.Where(x => x.ProjectId == project.Id);
                    foreach(var task in tasks)
                    {
                        var comments = db.Comments.Where(x => x.TaskId == task.Id);
                        foreach(var comment in comments)
                        {
                            db.Comments.Remove(comment);
                        }
                        db.Tasks.Remove(task);
                    }
                    db.Projects.Remove(project);
                }
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var organizerOrganizedTeams = db.Teams.Where(x => x.OrganizerId == team.OrganizerId);
                if (organizerOrganizedTeams.Count() == 1 )
                {
                    UserManager.RemoveFromRole(team.OrganizerId, "Organizer");
                }
                var organizerUserTeam = db.UserTeams.Where(x => x.TeamId == id && x.UserId == team.OrganizerId).FirstOrDefault();
                db.UserTeams.Remove(organizerUserTeam);
                var userTeams = db.UserTeams.Where(x => x.TeamId == id);
                foreach (var userTeam in userTeams)
                {
                    var thisUserMemberTeams = db.UserTeams.Where(x => x.UserId == userTeam.UserId).Select(x => x.TeamId).ToList();
                    var thisUserOrganizerTeam = db.Teams.Where(x => x.OrganizerId == userTeam.UserId).Select(x => x.Id).ToList();
                    if (thisUserMemberTeams.Count() - thisUserOrganizerTeam.Count() == 1 && !thisUserOrganizerTeam.Contains(id))
                    {
                        UserManager.RemoveFromRole(userTeam.UserId, "Member");
                    }
                    db.UserTeams.Remove(userTeam);
                }

                db.Teams.Remove(team);
                db.SaveChanges();
                TempData["message"] = "Echipa a fost sterasa!";
                return RedirectToAction("Index", "Team");
            }
            else
            {
                TempData["message"] = "Nu aveti dreptul sa stergeti o echipa care nu va apartine!";
                return Redirect("~");
            }

        }

    }
}
