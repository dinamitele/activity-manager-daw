﻿using ActivityManager.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using System.Collections.Generic;
using System.Linq;

[assembly: OwinStartupAttribute(typeof(ActivityManager.Startup))]
namespace ActivityManager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            // Se apeleaza o metoda in care se adauga contul de administrator si rolurile aplicatiei             
            createAdminUserAndApplicationRoles();
            // Se apeleaza o metoda in care se adauga statusurile
            createStatuses();
        }

        private void createAdminUserAndApplicationRoles()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            // Se adauga rolurile aplicatiei            
            if (!roleManager.RoleExists("Administrator"))
            {
                // Se adauga rolul de administrator                 
                var role = new IdentityRole();
                role.Name = "Administrator";
                roleManager.Create(role);

                // Se adauga utilizatorul administrator                 
                var user = new ApplicationUser();
                user.UserName = "admin@admin.com";
                user.Email = "admin@admin.com";

                // Al doilea parametru este parola
                var adminCreated = UserManager.Create(user, "Administrator1!");
                if (adminCreated.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "Administrator");
                }
            }

            ////folosim pentru a sterge rolurile noi
            //if (roleManager.RoleExists("member"))
            //{
            //    var role = roleManager.FindByName("member");
            //    roleManager.Delete(role);
            //}

            // se adauga utilizatorul "organizator" (care a creat un proiect)
            if (!roleManager.RoleExists("Organizer"))
            {
                var role = new IdentityRole();
                role.Name = "Organizer";
                roleManager.Create(role);
            }

            // se adauga utilizatorul "membru"  (care face parte dintr-o echipa)
            if (!roleManager.RoleExists("Member"))
            {
                var role = new IdentityRole();
                role.Name = "Member";
                roleManager.Create(role);
            }

            // se adauga utilizatorul simplu (care nu are niciun proiect creat si nici nu face parte dintr-o echipa)
            if (!roleManager.RoleExists("User"))
            {
                var role = new IdentityRole();
                role.Name = "User";
                roleManager.Create(role);
            }
        }

        private void createStatuses()
        {
            ApplicationDbContext db = ApplicationDbContext.Create();

            var statuses = from status in db.TaskStatus select status;
            List<string> statusNames = new List<string>();
            foreach (var status in statuses)
            {
                statusNames.Add(status.Status.ToString());
            }

            if (!statusNames.Contains("Not Started"))
            {
                TaskStatus taskStatus = new TaskStatus();
                taskStatus.Status = "Not Started";
                db.TaskStatus.Add(taskStatus);
                db.SaveChanges();
            }

            
            if (!statusNames.Contains("In progress"))
            {
                TaskStatus taskStatus = new TaskStatus();
                taskStatus.Status = "In progress";
                db.TaskStatus.Add(taskStatus);
                db.SaveChanges();
            }

            if (!statusNames.Contains("Completed"))
            {
                TaskStatus taskStatus = new TaskStatus();
                taskStatus.Status = "Completed";
                db.TaskStatus.Add(taskStatus);
                db.SaveChanges();
            }
        }
    }
}
