﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActivityManager.Models
{
    public class Task
    {
        [Key]
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int TaskStatusId { get; set; }
        [Required(ErrorMessage = "The Title field is required")]
        public string Title { get; set; }
        [Required(ErrorMessage = "The Description field is required")]
        public string Description { get; set; }
        public string CreatedById { get; set; }
        public string AssignedToId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public virtual Project Project { get; set; }
        public virtual TaskStatus TaskStatus { get; set; }
        public virtual ApplicationUser CreatedBy { get; set; }
        public virtual ApplicationUser AssignedTo { get; set; }


        // Se aduaga acest atribute pentru a putea prelua toate proiectele si statusurile
        public IEnumerable<SelectListItem> Projects { get; set; }
        public IEnumerable<SelectListItem> TaskStatuses { get; set; }
        public IEnumerable<SelectListItem> TeamPersons { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
    }
}
