﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ActivityManager.Models
{
    public class Project
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "The name field is required")]
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UserId { get; set; }
        public int TeamId { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual Team Team { get; set; }
        public virtual IEnumerable<Task> Tasks { get; set; }
    }
}
