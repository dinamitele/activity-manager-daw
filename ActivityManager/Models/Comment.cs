﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ActivityManager.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "The content field is required")]
        public string Content { get; set; }
        public int TaskId { get; set; }
        public string UserId { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public virtual Task Task { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}