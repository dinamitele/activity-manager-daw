﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActivityManager.Models
{
    public class UserTeam
    {
        [Key]
        [Column(Order = 1)]
        public string UserId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int TeamId { get; set; }

        public virtual ApplicationUser User { get; set; }
        public virtual Team Team { get; set; }

        // Se aduaga acest atribute pentru a putea prelua toate proiectele si statusurile
        public IEnumerable<SelectListItem> AllUsers { get; set; }
    }
}