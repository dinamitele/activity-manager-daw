﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActivityManager.Models
{
    public class Team
    {
        /*//pentru relatia many-to-many
        public Team()
        {
            ApplicationUser = new HashSet<ApplicationUser>();
        }*/

        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "The Name field is required.")]
        public string Name { get; set; }
        public string OrganizerId { get; set; }

        /*public int ProjectId { get; set; }
        public virtual Project Project { get; set; }*/

        public virtual ApplicationUser Organzier { get; set; }

        //Pentru many-to-many
        public virtual ICollection<UserTeam> UserTeams { get; set; }

        // Se aduaga acest atribute pentru a putea prelua toate proiectele si statusurile
        public IEnumerable<Task> Tasks { get; set; }
        public IEnumerable<Project> Projects { get; set; }
        //public virtual ICollection<ApplicationUser> ApplicationUser { get; set; }
    }
}