﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ActivityManager.Models
{
    public class TaskStatus
    {
        [Key]
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
