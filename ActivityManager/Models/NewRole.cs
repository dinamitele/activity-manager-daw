﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActivityManager.Models
{
    public class NewRole
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public int TeamId { get; set; }

        public IEnumerable<SelectListItem> AvailableMemberTeams { get; set; }
        public IEnumerable<SelectListItem> AvailableOrganizerTeams { get; set; }
        public IEnumerable<SelectListItem> AllRoles { get; set; }
    }
}